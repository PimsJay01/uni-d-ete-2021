## Firebase

### Introduction

Firebase est un ensemble de services d'hébergement pour différents types d'applications. Il propose d'héberger en NoSQL et en temps réel des bases de données, du contenu, de l'authentification, des notifications et une multitudes d'autres services intégrés et implémentable rapidement.

Documentation: https://firebase.google.com/docs

#### Notions

- Création d'un compte sur Firebase
- Mise à disposition de différents SDKs (Web, Angularfire, Android, iPhone)
- Différences entre Firebase et Google Cloud (Documentation différente)


### Command Line Interface

Le CLI Firebase offre une variété d'outils pour la gestion, la visualisation et le déploiement des projets Firebase.

Documentation: https://firebase.google.com/docs/cli

#### Notions

- Installation du CLI
- Login: `firebase login`
- Init: `firebase init`
- Fichier de configuration: `firebase.json`
- Déploiement: `firebase deploy`


### Authentification

Firebase Authentication fournit des services backend, des SDK faciles à utiliser et des bibliothèques d'interface utilisateur prêtes à l'emploi pour authentifier les utilisateurs auprès de votre application

Documentation: https://firebase.google.com/docs/auth

#### Notions

- Interface de gestion
- Types d'authentifications


### Firestore

base de données cloud NoSQL flexible et évolutive pour stocker et synchroniser les données pour le développement côté client et côté serveur.

Documentation: https://firebase.google.com/docs/firestore

#### Notions

- Modèle de données (documents)
- Aperçu de l'API
- Synchronisation des données
- Cache automatique
- Règles de sécurités
- Indexes


### Cloud functions

Le Cloud Functions est un framework sans serveur qui permet d'exécuter automatiquement du code backend en réponse à des événements. Le code JavaScript ou TypeScript est stocké dans le cloud de Google et s'exécute dans un environnement géré.

Documentation: https://firebase.google.com/docs/functions

#### Notions

- Types d'événements
- Contexte & authentification
- Traitement des données
- Emulateur local `firebase serve`
- Tests unitaires

### Autres services

- Analytics
- Buckets
