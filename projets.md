## Liste des projets des étudiants

Liste des projets validés par les professeurs

### Equipe 1 - SIK

Gestion du budget

PoC: Auth + Lecture données dans Firestore pour afficher des graphiques
MVP: Création de toutes les interface de l'App sans logique pour tester des scénarios 

**Constitution de l'équipe:**

- Ilias N'hairi
- Kiady Arintsoa
- Simon Cirill


### Equipe 2 - BAD

**BikePark**

PoC: PoC + Fonctionnement de la cartographie + Géolocalisation
MVP: Flyers + Landing page pour mesurer e-mail ou téléchargements

**Constitution de l'équipe:**

- Denis Gremaud
- Bedran Sezer
- Aya Mouzahim


### Equipe 3 - Buggy soft

Tower defense

PoC: Changement de map avec animation et effets particules
MVP: PoC sur itchi.io pour récupérer des mails de joueurs intéressés 

**Constitution de l'équipe:**

- Dubelly William
- Ortlieb Matthiew


### Equipe 4 - Kekos

Jeu de stratégie tour par tour

PoC: Chemin le plus court sur une carte hegagonale avec obstacles et brouillard de guerre
MVP: Page steam et mesure des wishlists

**Constitution de l'équipe:**

- Pertuzari Gustavo
- Paulot Alexey


### Equipe 5 - Zoulou-Binks

Jeu de plateau impossible

*... Description du PoC ...*

**Constitution de l'équipe:**

- Dogier Thomos
- Bernasconi Dorian


### Equipe 6 - Big Chungus

Gamifier la lecture

*... Description du PoC ...*

**Constitution de l'équipe:**

- Perret Xavier
- Nguyen Trung
- Blancy Antoine


### Equipe 7 - Booby team

**Optimum l'agenda++**

PoC: Agenda interactif avec vue clendrier
MVP: Implémentation des vues sur FLutter sans logique avec données fictives

**Constitution de l'équipe:**

- Romain Peretti - romain.peretti@etu.hesge.ch
- Thibaut Michaud - thibaut.michaud@etu.hesge.ch


### Equipe 8 - Dream team

**AnimalGarden**
Airbnb de la garde de maison/animaux

MVP: Landing page pour mesurer la création de compte
PoC: Filtre et résultats sur carto

**Constitution de l'équipe:**

- Julien Fäh
- Julian Rühl
- Lometti Fabien (n'a pas terminé)

