## Présentations

*à compléter*

Ce fichier décrit les attentes des enseignants concernant la présentation finale.

### Structure de la présentation

- Problématique ou besoin auquel le projet répond
- Vision long-terme du projet
- Présentation de la réalisation du PoC
- Présentation de la réalisation du MVP (si débuté)
- Difficultés rencontrées
- Retrospective et enseignements tirés

### Critères d'évaluation

- Documentation
- Qualité de la présentation
- Respect de la structure de la présentation
- Atteintes des objectifs de l'équipe et des enseignants
- Aucun plagiat (Les sources doivent-être mentionnés dans le code)
- Qualité de code (fonctionnelle et structurelle)

...
- Mise en place de la sécurisation des communications
- Tests unitaires ?
- ...
