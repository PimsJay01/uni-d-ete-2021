## Uni d'été 2021

L'uni d'été 2021 est consacrée à la réalisation et au déploiement d'une application Web/mobile en Dart avec le SDK flutter ou d'une application Web Typescript avec le moteur de jeu Phaser. Des notions liées au déploiement dans le Cloud et à la gestion de projet sont introduitent aux étudiants afin de leur donner les outils qui leur permettront de maximiser le succès de leurs projets futurs.

L'application réalisée pendant l'uni d'été est développée par équipe de 3 étudiants constituée selon leur choix dans la mesure du possible et obligatoirement validée par les enseignants **avant le mercredi 1er septembre**. Une limite de 10 équipes maximums est fixées pour le bon déroulement de l'uni d'été. En conséquence, les équipes de 3 étudiants seront validées en priorité.

### Frontend

L'application doit-être conçue avec l'utilisation de l'un des 2 frameworks frontends suivants:

- [SDK Flutter/Dart](https://flutter.dev/) - Framework de développement Web & mobile par Google
- [Phaser.js](https://phaser.io/) - Framework de développement de jeux-vidéo avec HTML5

### Hébergement

[Firebase de Google Cloud](https://firebase.google.com/) doit-être utilisé pour l'implémantation du projet. En particulier les briques suivantes:

- [Authentification](https://firebase.google.com/docs/auth) - SDK de gestion des identités
- [Déploiement](https://firebase.google.com/docs/cli) - Déploiement de l'application dans le cloud
- [Firestore](https://firebase.google.com/docs/firestore) - Base de données NoSQL
- [Cloud functions](https://firebase.google.com/docs/functions) - Servelerless

### Méthodologies

Afin de réaliser cette application dans les meilleures conditions, des ateliers et notions méthodologiques sont introduites pendant la semaine 1:

- Idéation
- Design Studio
- Vision
- Storymap
- Roadmap
- Pitch du concept

### Instructions complémentaire

La présence des étudiants en classe est requise lors de la première semaine d'uni d'été ainsi que tous les vendredi. Toute absence devra être annoncée et justifiée.

### Critères pour le projet

[Des projets exemples](/projets-exemples.md) sont proposés aux étudiants mais ceux-cis peuvent réaliser leur propre projet tant que celui-ci respecte les contraintes suivantes:

**Le projet doit poser les bases d'une vision à long-terme**

Sur les bases de cette vision, un PoC et un MVP doivent-être identifiés et justifiés par les étudiants:

- Identification d'un PoC stratégique et cohérent avec la vision à réaliser lors de l'uni d'été.
- Identification d'un MVP composé exclusivement des fonctionnalités essentielles.
- Le PoC et MVP identifiés doivent être réalisables sur le temps alloué durant l'uni d'été.

![Think big, start small, learn fast](img/startup.jpg)

#### Proof of Concept (PoC)

Une **preuve de concept** ou **démonstration de faisabilité**, est une réalisation ayant pour vocation de montrer la faisabilité d'un procédé ou d'une innovation.
Située très en amont dans le processus de développement d'un produit ou d'un processus nouveau, la preuve de concept peut être considérée comme une étape essentielle selon la nature du projet.

**Question à se poser:** *Quelle complexité technique dois-je développer en amont pour m'assurer la faisabilité du projet ?* 

#### Minimum Viable Project (MVP)

Dans le cadre de la conception de produit, le **produit minimum viable** est la version d'un produit qui permet de générer un maximum de valeur avec un minimum d'effort.
Par extension, il désigne aussi la stratégie utilisée pour fabriquer, tester et mettre sur le marché ce produit le plus rapidement possible à moindre coût.

Le MVP permet de valider rapidement de satisfaire au 3 piliers de l'innovations essentiels à la réussite d'un produit.

![3 pilliers de l'innovation](img/innovation.jpg)

**Question à se poser:** *Quelle sont les fonctionnalités essentielles ? Est-ce que je peux simuler, substituer, simplifier, contextualiser, etc. certaines fonctionnalités pour tester au plus vite une solution innovante ?*

Voir les [slides présentées en cours](https://gitlab.com/PimsJay01/uni-d-ete-2021/-/blob/master/uni-d-ete-2021-Atelier-DT-MVP-Cadrage.pdf)

## Planning

Planning des 3 semaines de l'uni d'été du lundi 30 août au 17 septembre.

![Planning](img/planning.png)

### Semaine 1

#### Lundi 30 août

*Journée théorique*

- Objectifs de l'uni d'été
- Présentation du planning
- Présentation de [Flutter](/flutter.md) 


#### Mardi 31 août

*Journée théorique*

- Correction exemple [Flutter](/flutter.md) 
- Refresh [TypeScript](/typescript.md) 
- Présentation de [Phaser.js](/phaserJS.md)


#### Mercredi 1 septembre

*Journée théorique et pratique*

- Présentation de [Firebase](/firebase.md)
- Ateliers de cadrage du projet


#### Jeudi 2 septembre

Préparation pour le pitch et validation des projets

##### Pitch

Le pitch doit décrire votre projet ainsi que le travail effectué pendant le cadrage
*15 minutes par équipe*

**Structure du pitch**

- Problématique ou besoin auquel le projet répond
- Vision long-terme du projet
- Description du PoC
- Présentation du MVP et de ses fonctionnalités
- Choix technologiques, architecture et croquis des écrans
- Roadmap semaine 2 et semaine 3

**Heures de passages des équipes:**

1. 13h30 - équipe 1 - SIK
2. 13h55 - equipe 2 - BAD
3. 14h20 - équipe 3 - Bugisoft
4. 14h45 - équipe 4 - Kekos
5. 15h10 - équipe 5 - Zoulou-binks
6. 15h35 - équipe 6 - Big Chungus
7. 16h00 - équipe 7 - Booby Team
8. 16h25 - équipe 8 - Dream Team

**Les étudiants doivent transmettre aux enseignants le lien vers le dépôt de leur projet contenant une brève description du projet avant la fin de la semaine .**

Merci d'ajouter les droits sur le dépôt afin que les enseignants puissent accéder à vos projets.

@PimsJay01, @raed22 et @stephane.malandain

Ces projets sont ajoutés par la suite à la [liste des projets validés](/projets.md) *à compléter par les enseignants*

#### Vendredi 3 septembre

Début de la conception du projet


### Semaine 2

#### Vendredi 9 septembre

Discussion de 15 minutes entre les enseignants et chaqu'une des équipes

**Heures de passages des équipes:**

1. 13h30 - équipe 5 - Zoulou-binks
2. 13h50 - equipe 6 - Big Chungus
3. 14h10 - équipe 7 - Booby Team
4. 14h30 - équipe 8 - Dream Team
5. 14h50 - équipe 1 - SIK
6. 15h10 - équipe 2 - BAD
7. 15h30 - équipe 3 - Bugisoft
8. 15h50 - équipe 4 - Kekos

### Semaine 3

#### Jeudi 15 septembre

Rendu des projets **avant le vendredi 16 septembre**.

#### Vendredi 16 septembre

Présentations finales

- [Structure et critères d'évaluation](/presentation.md) *à compléter par les enseignants*

**Heures de passages des équipes:**

1. 13h30 - équipe 8 - Dream Team
2. 13h55 - equipe 7 - Booby Team
3. 14h20 - équipe 6 - Big Chungus
4. 14h45 - équipe 5 - Zoulou-binks
5. 15h10 - équipe 4 - Kekos
6. 15h35 - équipe 3 - Bugisoft
7. 16h00 - équipe 2 - BAD
8. 16h25 - équipe 1 - SIK

## Evaluations

Liste des éléments évalués lors de l'uni d'été:

- Pitch
- Présentation intermédiaire
- Présentation finale
- Rendu du projet
