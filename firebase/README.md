## Google Cloud Firebase

### Créez un nouveau projet sur Firebase

https://firebase.google.com/

### Activez les services

Activez au minimum dans l'interface de gestion de Firebase **Firestore Database**.
Vous pouvez en utilisez d'autres suivant les besoins de votre projet.
Functions fait partie du *Blaze plan" payant mais cela ne vous sera pas facturer tant que vous ne deppassez pas les limites fixées dans ce document: https://firebase.google.com/pricing/
Choisissez la région **europe-west6** pour héberger vos services à Zurich.

### Exploiter les services Firebase

Pour utiliser les services Firebase et déployer votre projet, suivez l'assistant que vous trouverez sur l'interface de gestion de Firebase: Configuration du projet -> Général -> Ajouter Firebase à votre Application Web 

![Setup](setup.png)

Pour en savoir plus: https://firebase.google.com/docs/web/setup 

#### Ajouter le SDK à votre projet

`npm install firebase`

#### Installez le CLI Firebase

`npm install -g firebase-tools`

https://firebase.google.com/docs/cli

#### Initialisez Firebase

Authentifiez-vous avec le CLI Firebase:

`firebase login`

Avant de lancer la configuration de Firebase identifiez ou ce trouve le dossier des scripts compilé que vous souhaitez déployer dans l'hébergement cloud. Dans ce projet il s'agit du dossier build contenant index.html.

Initialisez le projet à la racine du dossier de votre projet:

`firebase init`

1. Sélectionnez (avec espace) à minima: Firestore et Hosting/Hébergement.
2. Utilisez un projet existant et sélectionner votre projet dans la liste.
3. Laissez le nom par défaut des fichiers de configuration: **firestore.rules** et **firestore.indexes.json**.
4. Modifiez le nom du dossier **public** par le dossier contenant votre build.
5. Confirmez par oui (y) pour ré-écrire toutes les URLs vers index.html.
6. Infirmez par non (N) la configuration automatique du build depuis Github.
7. Faite en de mêmem (N) afin de ne pas écrasser le fichier index.html contenu dans votre dossier contenant le build.

Firebase a créé 3 fichiers:

- **firebase.json**: contient la configuration de Firebase que vous venez d'initialiser.
- **firestore.rules**: contient les règles de sécurité pour manipuler les données de la base Firestore.
- **firestore.indexes.json**: contient les indexes de la base de données que Firebase vous demandera de générer en fonction des requêtes que vous ajouterez dans votre projet.

#### Déployer le projet

`firebase deploy`

Firebase déploie votre projet et vous affiche à la fin l'URL pour visualiser votre Application Web.

Exemple: https://uni-d-ete-2021.web.app

### Authentification

Allez dans la partie Authentification de l'interface de gestion de Firebase et activez à minima la connexion par e-mail et ajoutez un utilisateur pour chaque membre de votre équipe.

Pour la suite référez vous à la documentation: https://firebase.google.com/docs/auth

### Firebase

Référez vous à la documentation: https://firebase.google.com/docs/firestore
