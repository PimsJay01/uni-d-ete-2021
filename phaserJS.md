## Phaser.JS

Framework de développement de jeux-vidéo avec HTML5

branche Git: phaserJS

*Disclaimer: La documentation de Phaser JS est seulement en anglais*

### Introduction

Introduction à Phaser.JS

Documentation: https://phaser.io/
Dépot Git: https://phaser.io/download

#### Notions

- Canvas vs WebGL
- Basé sur Pixi.JS

### Installation

Installation de Phaser.JS

Documentation: https://photonstorm.github.io/phaser3-docs/index.html#toc1__anchor

#### Notions

- NPM & Package.json
- Typescript & tsconfig.json
- Webpack

### Game Configuration object

Configuration de Phaser JS

Documentation: https://photonstorm.github.io/phaser3-docs/index.html#toc7__anchor

#### Notions

- Type: Phaser.AUTO
- Physic
- Scene

### Preload & Create

Cycle de vie de Phases

Documentation: https://photonstorm.github.io/phaser3-docs/index.html#toc7__anchor
Dépot Git : https://gist.github.com/photonstorm/46cb8fb4b19fc7717dcad514cdcec064

#### Notions

- Load + Add
- Particules
