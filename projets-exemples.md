## Liste des projets exemples

### Projets Web - Floater.js ou React.js

*à compléter par @stephane.malandai*


#### Nom du projet

*... Vision du projet en 1-2 phrases ...*

*PoCs en rapport avec ce projet:*

*... Description du ou des PoCs ...*

*MVP à définir selon la vision*


### Projets de jeu - Phaser.js

Proposition de 2 projets à réaliser avec le framework Phaser.js

#### Jeu de carte

Vision: Développement d'un jeu de carte à la Hearthstone, Gwent ou Yugiho.

*PoCs en rapport avec ce projet:*

- Système d'animation des cartes (visualisation du deck, collecte d'une carte, cartes en main, zoom sur une carte, mise en jeu d'une carte)
- Animation d'illustration 2d avec l'utilisation d'un squelette pour un effet à la Harry Potter
- Animation de l'effet d'une carte (particules, sources de lumières et autres effets de coloration)

*MVP à définir selon la vision du jeu à réaliser*

#### Jeu de stratégie 

Vision: Développement d'un jeu de stratégie de combat tour par tour du type Battle Brothers, Advance Wars ou Fire Emblem.

*PoCs en rapport avec ce projet:*

- Mise en place d'une carte 2d hexagonale et déterminer le chemin de plus cours
- Animation d'une liste d'action 2d avec l'utilisation d'un squelette transposable sur différentes unitées
- Animation du contexte météorologique (particules, sources de lumières et autres effets de coloration)

*MVP à définir selon la vision du jeu à réaliser*
